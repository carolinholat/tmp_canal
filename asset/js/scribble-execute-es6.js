'use strict';

import $ from '../../bower_components/jQuery/dist/jquery.min';
// Canal: Asset Polyfill
import '../../vendor/flood/canal-asset/polyfill/object-assign';
import '../../vendor/flood/canal-asset/polyfill/node-html-foreach';
// Fetch Polyfill
// https://github.com/github/fetch
import '../../bower_components/fetch/fetch';
// Canal: Asset Utils
import '../../vendor/flood/canal-asset/util/Array';
// Canal: Asset Ressources
import Ready from '../../vendor/flood/canal-asset/src/Ready';
import LazyLoad from '../../vendor/flood/canal-asset/src/LazyLoad';
import StateMachine from '../../vendor/flood/canal-asset/src/StateMachine';
import AnimationFlow from '../../vendor/flood/canal-asset/src/AnimationFlow';
//import TemplateFetch from '../../vendor/flood/canal-asset/src/TemplateFetch';
import Gdpr from '../../vendor/flood/canal-asset/module/gdpr/Canal-GDPR';

//import Menu from './src/menu';

window.$ = $;
window.jQuery = $;

/**
 *
 * @type {TemplateFetch}
 */
/*window.tf = new TemplateFetch({
    host: window.location.protocol + '//' + window.location.host + '/',
    debug: true
});*/

new Ready(() => {
    //let menu = new Menu();
    //menu.bind();

    let gdpr = new Gdpr({
        debug: true,
        selector: 'gdpr-autoload'
        //autoload: true,
    });

    // Fetch and display the pre-rendered `view/templatefetch/overlay.twig`
    /*tf.fetch('overlay', {
        css: 'example-overlay',
        btn: 'Close',
        content: '<p>Some Content!</p>',
    }).then(function (response) {
        document.querySelector('.container--inner').insertAdjacentHTML('beforeend', response.html);
    });*/



    let sm = new StateMachine({
        debug: true,
        current: 'closed',
        states: {
            closed: {
                on: {
                    leaving(current, next) {
                        console.log('ooon leaving');
                    },
                    leave(current, next) {
                        console.log('ooon leave');
                    },
                    leaved(current, next) {
                        console.log('ooon leaved');
                    },
                    entering(current, next) {
                        console.log('ooon entering');
                    },
                    enter(current, next) {
                        console.log('ooon enter');
                    },
                    entered(current, next) {
                        console.log('ooon entered');
                    }
                }
            },
            opening: {
                on: {
                    leaving(current, next) {
                        console.log('ooon leaving');
                    },
                    leave(current, next) {
                        console.log('ooon leave');
                    },
                    leaved(current, next) {
                        console.log('ooon leaved');
                    },
                    entering(current, next) {
                        console.log('ooon entering');
                    },
                    enter(current, next) {
                        console.log('ooon enter');
                    },
                    entered(current, next) {
                        console.log('ooon entered');
                    }
                }
            },
            open: {},
            closing: {}
        },
        on: {
            leave(current, next) {
                console.log('transiiiiition ooon leave');
            },
            enter(current, next) {
                console.log('transiiiiition ooon enter');
            }
        },
        // the transition events are only done when exactly the same transition happens, the transition is the arrow
        transition: {
            'closed->opening': (current, next) => {
                console.log('transition: closed-opening');
            }
        }
    });

    setTimeout(() => {
        console.log('opening');
        sm.go('opening');
        /*setTimeout(() => {
            console.log('open');
            sm.go('open');
            setTimeout(() => {
                console.log('closing');
                overlay_animation.go('closing');
                setTimeout(() => {
                    console.log('closed');
                    overlay_animation.go('closed');
                }, 2000);
            }, 2000);
        }, 2000);*/
    }, 0);
    //}, 2000);

    // scribble for animation flow
    let overlay_animation = new AnimationFlow({
        node: document.body.querySelector('.nav-service'),
        debug: true,
        current: 'closed',
        class_prefix: 'overlay--',
        states: {
            closed: {
                class: [],
            },
            opening: {
                class: [
                    'opening',
                    'visible',
                ]
            },
            open: {
                class: [
                    'open',
                    'visible'
                ]
            },
            closing: {
                class: [
                    'closing',
                    'visible'
                ]
            }
        },
        // the transition events are only done when exactly the same transition happens, the transition is the arrow
        transition: {
            'open->closing': {
                on(current, next, AnimationFlow) {
                    console.log('transition: closed-opening');
                }
            }
        }
    });
    setTimeout(() => {
        overlay_animation.go('opening');
        setTimeout(() => {
            overlay_animation.go('open');
            setTimeout(() => {
                overlay_animation.go('closing');
                setTimeout(() => {
                    overlay_animation.go('closed');
                }, 2000);
            }, 2000);
        }, 2000);
    }, 2000);

    new LazyLoad({
            debug: true,
            onetime: true,
        },
        // callback when one element comes visible
        (elem) => {
            gdpr.create(elem);
        })
    // start observing all not already rendered autoload items
        .observe(document.querySelectorAll(gdpr.selector.element + ':not(' + gdpr.selector.rendered + ')'));

});