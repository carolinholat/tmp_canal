# Development Tools Folder

This folder contains multiple development tools and examples of those.

# Grunt

Grunt is an easy way, preconfigured for Sass, Media Files and < ES6 JS.
It could be run from the folder it is in.

> The JS `execute.js` file is legacy and does nothing, will be removed in the future.
> As Grunt at all will be removed.

# Webpack / >ES6

Webpack is in folder `wp`, will have multiple configuration files for different tasks.

At the moment the JS Task uses Babel for > ES6 JS dev.
Sadly babel needs to have some npm packages in the root directory, the `/package.json` needs to have `babel-preset-env` and `babel-preset-es2015` when importing e.g. a bower_component or something from another folder that is not below of this folder (so, everything).

# Gulp

A few tasks will not be moved to webpack, instead will be existing as Gulp tasks.