<?php

$option['run']['begin']['time'] = microtime(true); // Set time value for performance monitor
$option['run']['begin']['memory'] = memory_get_usage(); // Set memory value for performance monitor

define('CAPTN_IS_STEERING', true);

require_once __DIR__.'/vendor/flood/captn/autoload.php';
require_once __DIR__.'/vendor/autoload.php';

use Flood\Canal\Frontend;
use Flood\Component\PerformanceMonitor\Monitor;

Monitor::i()->startProfile('all', $option['run']);

$frontend = new Frontend();

bootFrontend($frontend);

$frontend->display();

Monitor::i()->endProfile('all', $option['run']);
// Monitor::i()->getInformation('all')['all']['time'];
if('some-route-id' !== $frontend->match['_route']) {
    error_log('Canal: run time of ' . Monitor::i()->getInformation('all')['time']);
}