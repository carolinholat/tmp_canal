<?php


namespace Flood\Canal\App;

use Flood\Canal\AppService\PageController;
use Flood\Canal\Frontend;

class ErrorController extends PageController {
    /**
     * Home constructor.
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct($frontend) {
        try {
            parent::__construct($frontend);
        } catch(\Exception $e) {

        }


        /*
         * A demo AppService.method
         */
        // set view data defaults like menu and gdpr settings
        $this->view();
    }

    /**
     * @param Frontend $frontend
     * @param $template
     * @param $errorMessage
     */
    public static function getErrorController($frontend, $template, $errorMessage) {
        $controller = new self($frontend);
        $controller->setError($errorMessage);
        echo $controller->render($template);
    }

    public function setError($error) {

        if($this->frontend->debug) {
        }
        $this->assign('errorMessage', $error);
    }

}
