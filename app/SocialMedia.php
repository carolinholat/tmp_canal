<?php

namespace Flood\Canal\App;

use Flood\Canal\AppService\PageController;

class SocialMedia extends PageController {

    /**
     * Home constructor.
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
        $this->path_template = 'SocialMedia.twig';

        /*
         * A demo AppService.method
         */
        $this->view();
    }
}