<?php

namespace Flood\Canal\App;

use Flood\Canal\Controller\Base;

class DemoController extends Base {
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
    }
}