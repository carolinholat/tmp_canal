<?php

use Flood\Canal\App\ErrorController;
use Flood\Canal\Feature\User\Token;

/**
 * @param $frontend \Flood\Canal\Frontend
 */
return function($frontend) {
    $frontend->debug = true;

    /**
     * @var string $host only allow routes from this host, when not another is defined within routes. ['host'=>'val']
     */
    $frontend->host = '';
    // when using dev server ssl is off, when production it is on
    $frontend->ssl = (php_sapi_name() == 'cli-server' ? false : true);

    $frontend->path_tmp = __DIR__ . '/tmp/';

    $frontend->path_data = __DIR__ . '/data/';

    // setup all Twig view dirs
    $frontend->addViewDir(__DIR__ . '/view/')
             ->addViewDir(__DIR__ . '/vendor/flood/canal-view/src')
             ->addViewDir(__DIR__ . '/data/content/_block', 'block');

    //
    // Add Features

    // storage
    $frontend->feature->add(require __DIR__ . '/vendor/flood/canal/feature/Storage/_feature.php');

    // route
    $route_config = [
        'debug'    => false,
        'api_base' => '/api',
    ];
    $route_feature = require __DIR__ . '/vendor/flood/canal/feature/feature-route.php';
    $frontend->feature->add($route_feature($route_config));

    // canal user
    $frontend->feature->add(require __DIR__ . '/vendor/flood/canal/feature/User/_feature.php');
    Token::$max_age = 30000;
    Token::$jwt_secret = 'vlQmc!:KcN]QpbxhJ1+RKzY,v%,$<cSA';

    // canal cms
    $frontend->feature->add(require __DIR__ . '/vendor/flood/canal/feature/Content/_feature.php');

    // twig
    $twig_config = [
        // If twig should check if need to re-cache, recommended to turn off in production
        'twig_auto_reload' => true,
    ];
    $twig_feature = require __DIR__ . '/vendor/flood/canal/feature/feature-twig.php';
    $frontend->feature->add($twig_feature($twig_config));

    // file manager (user upload files)
    $frontend->feature->add(require __DIR__ . '/vendor/flood/canal/feature/FileManager/_feature.php');

    // api
    $api_config = [
        'origin_allowed' => [
            'http://localhost:3000',
            'http://localhost:25021',
            'https://admin.demo.canal.bemit.codes',
            'https://nightly.admin.demo.canal.bemit.codes',
        ],
    ];
    $api_feature = require __DIR__ . '/vendor/flood/canal/feature/Api/_feature.php';
    $frontend->feature->add($api_feature($api_config));

    //add an errorcontrollerlist comprehending all errorcontrollers with all exception-messages
    $frontend->addErrorControllerList([
        'Unauthorized' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', '401: Unauthorized');
        },
        'PaymentRequired' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', '402: Payment required');
        },
        'ResourceNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', '404 Resource');
        },
        'ContentNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', '404 Content');
        },
        'MethodNotAllowed' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', '405: Method not allowed');
        },
        'ControllerDoesNotReturnResponse' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Controller does not return response');
        },
        'Forbidden' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', '403: Access forbidden');
        },
        //throws in Feature/Content/Article
        'ArticleRawContentNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article: raw content not found');
        },
        'ArticleParserNotSet' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article: parser not set');
        },
        'ArticleRendererNotSet' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article: renderer not set');
        },
        'ArticleRawContentEmpty' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article: raw content empty');
        },
        //throws in Feature/Content/DocTree/BlockRenderer
        'ArticleTreeFragmentRenderBlockHasNoData' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article Tree Fragment: renderBlock has no data');
        },
        'ArticleTreeFragmentRenderBlockHasNoId' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article Tree Fragment: renderBlock has no ID');
        },
        //catches errors that are not caught anywhere else
        'NoErrorControllerFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'No errortype matching');
        },

        //ALL THE EXCEPTIONS THAT RETURN TO ACCESS FORBIDDEN:
        //throws in Feature/Storage/Json
        'JsonReadDecodedListWasNotArray' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: read decoded list with items has wrong type - is not an array');
        },
        'JsonReadCouldNotReadListWithItem' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: read could not read list with item');
        },
        'JsonReadCouldNotFindListWithItem' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: read could not find list with item');
        },
        // catches in Feature/User/RightManager
        'RightManagerValueNotAString' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'RightManager: value isset but not a string');
        },
        'RightManagerNoTaskInfo' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'RightManager: no task-info found for task');
        },

        // ALL THE EXCEPTIONS THAT ARE NOT THROWN BECAUSE METHOD IS NOT CALLED
        //throws in Feature/Content/DocTree/BlockRenderer
        'ArticleTreeFragmentRenderMetaHasNoData' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article Tree Fragment: renderMeta has no data');
        },
        //throws in Feature/Content/PageTypeStore
        'PageTypeStoreJsonNotReadable' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'PageTypeStore: json not readable');
        },
        'PageTypeStoreFileNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'PageTypeStore: file not found');
        },
        //throws in Feature/Content/SchemaStore
        'SchemaJsonNotReadable' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Schema: schema-json not readable');
        },
        'SchemaFileNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Schema: schema-file not found');
        },
        //throws in Feature/Storage/Json
        'JsonReadListCouldNotFindList' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: readList could not find list');
        },
        'JsonReadCouldNotWriteList' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: read could not write list');
        },
        'JsonCanNotCreateExistingList' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: can not create existing list');
        },
        'JsonCreateItemWrongDataType' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: create item - wrong type of data for item in list - is not an array');
        },
        'JsonCreateItemCouldNotWriteItemInList' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: create item - could not write item in list');
        },
        'JsonCreateItemCannotCreateExistingItem' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: create item - can not create existing item in list');
        },
        'JsonUpdateItemWrongDatatype' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: update item - wrong type of data for item in list - is not an array');
        },
        'JsonUpdateItemCouldNotGetData' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: update item - could not get existing data for item in list');
        },
        'JsonCanNotUpdateNonExistingItem' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: update item - cannot update non-existing item in list');
        },
        'JsonCanNotDeleteList' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: cannot delete list - is not a folder');
        },
        'JsonCanNotDeleteItem' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Json: cannot delete item - is not a file');
        },
        //throws exceptions from Feature/User/RightManager
        'RightManagerTaskInfoUser' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'RightManager: task info of user is not null and not bool');
        },
        'RightManagerTaskInfoGroup' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'RightManager: task info of group is not null and not bool');
        },
        //throws exception from Flood/Canal/Src/Console
        'ConsoleScope' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Console: registered scope is not an object');
        },
        //throws exception from Feature/Content/DocTree
        'DocTreeArticleDocTreeFileNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'ArticleDocTree: file not found');
        },

        // ALL THE EXCEPTIONS THAT ARE NOT CAUGHT AND LEAD TO FATAL ERROR
        //throws in Feature/Content/Section
        'SectionDuplicateArticleId' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Canal: Duplicate article-ID');
        },
        //throws in Feature/Content/DocTree/BlockStore
        'ArticleBlockFileNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article Block: File was not found');
        },
        'ArticleBlockJsonNotReadable' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article Block: json-file was not readable');
        },
        //throws in Feature/Content/ArticleMeta
        'ArticleMetaFileNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'ArticleMeta: meta-file not found');
        },
        'ArticleMetaMetaJsonNotReadable' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'ArticleMeta: meta-json not readable');
        },
         //throws exceptions from Flood/Canal/Src/Feature/Manager
        'FeatureManagerFeatureDoesntImplementFilesOrInterface' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'FeatureManager: featire does not implement Feature\File or its interface Feature\FileI');
        },

        //CONTENT IS NOT DISPLAYED BUT NO ERROR
        //throws in Feature/Content/DocTree
        'DocTreeJsonNotReadable' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article-Tree: json not readable');
        },
        'DocTreeFileNotFound' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article-Tree: file not found');
        },

        // ARE NOT USED AT ALL, SHOULD BE THROWN IN in Feature/Content/Article
        'ArticleTreeCouldNotBeSaved' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article: tree-file could not be saved');
        },
        'ArticleMetaCouldNotBeSaved' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article: meta could not be saved');
        },
        'ArticleMaintextCouldNotBeSaved' => function($frontend) {
            Flood\Canal\App\ErrorController::getErrorController($frontend, 'ErrorTemplate.twig', 'Article: maintext could not be saved');
        }
    ]);
};
