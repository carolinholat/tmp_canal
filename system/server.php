<?php

$request = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_URL);

if(false === $request) {
    error_log('Canal Server: REQUEST_URI is not valid.');
    exit(100);
}

if(null === $request) {
    error_log('Canal Server: REQUEST_URI is not set.');
    exit(101);
}

if(!is_string($request)) {
    error_log('Canal Server: unknown REQUEST_URI problem.');
    exit(102);
}

if(
    false === strpos($request, '/api/') && (
        preg_match('/\.(?:png|jpg|jpeg|gif)$/', $request) ||
        preg_match('/\.(?:pdf)$/', $_SERVER["REQUEST_URI"]) ||
        preg_match('/\.(?:eot|woff2|woff|ttf|svg)$/', $request)
    )
) {
    return false;
}

$filename = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_UNSAFE_RAW);

if(false === $filename) {
    error_log('Canal Server: REQUEST_URI is not valid.');
    exit(110);
}
if(null === $filename) {
    error_log('Canal Server: REQUEST_URI is not set.');
    exit(111);
}

if(!is_string($filename)) {
    error_log('Canal Server: unknown REQUEST_URI problem.');
    exit(112);
}

if(0 === strpos($filename, '/data/out/') && preg_match('/\.(?:css|js|css\.map|js\.map)$/', $filename)) {
    $path = pathinfo('../' . $filename);
    if($path['extension'] == 'js') {
        header('Content-Type: application/javascript');
        readfile('../' . $filename);
        exit(0);
    }

    if($path['extension'] == 'css') {
        header('Content-Type: text/css');
        readfile('../' . $filename);
        exit(0);
    }

    if($path['extension'] == 'map') {
        readfile('../' . $filename);
        exit(0);
    }
}

if(php_sapi_name() == 'cli-server') {
    error_log('Canal: Running on the CLI SAPI');
}
require dirname(__DIR__) . '/flow.php';

//WEBSOCKET
error_reporting(E_ERROR);
set_time_limit (1);

$host = 'localhost';
$port = 25021;

$sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_bind($sock, $host, $port);
socket_listen($sock);
$sockets = array($sock);
$arClients = array();
if (count($sockets) > 0) {
    error_log('socket ist existing');}

if (!is_resource($sock))
    error_log("socket_create() failed: ".socket_strerror(socket_last_error()), true);

if (!socket_bind($sock, $host, $port))
    error_log("socket_bind() failed: ".socket_strerror(socket_last_error()), true);

if(!socket_listen($sock, 20))
    error_log("socket_listen() failed: ".socket_strerror(socket_last_error()), true);

socket_set_nonblock($sock);

while (true){

    $sockets_change = $sockets;
    // müsste die variable nicht vielmehr bei except übergeben werden?
    $ready = socket_select($sockets_change, $write = null, $expect = null, 5);
    if (count($sockets_change) === 0) {
    error_log('no socket changed');}
    foreach($sockets_change as $s)
    {
        if ($s == $sock)
        {
// Änderung am Serversocket
            $client = socket_accept($sock);
            $sockets[] = $client;
            print_r($sockets);
            error_log('client added successfully to array');
        }
        else
        {
            error_log('client connection could not be added to array');
// Eingehende Nachrichten der Clientsockets
            $bytes = @socket_recv($s, $buffer, 2048, 0);
        }
    }
    break;

    }

error_log('the function stopped');
